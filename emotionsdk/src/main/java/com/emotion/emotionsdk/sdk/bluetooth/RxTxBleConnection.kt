package com.emotion.emotionsdk.sdk.bluetooth

import android.bluetooth.BluetoothGatt
import android.content.Context
import android.util.SparseArray
import com.emotion.emotionsdk.sdk.byteUtils.ByteArrayUtils
import com.emotion.emotionsdk.sdk.byteUtils.joinByteArray
import com.emotion.emotionsdk.sdk.byteUtils.logD
import java.util.*

class RxTxBleConnection(context: Context) : BleConnection(context) {

    private val actions: Deque<Action> = ArrayDeque()
    private val rxMap: SparseArray<(ByteArray) -> Unit> = SparseArray()
    private val rxFailMap: SparseArray<(ByteArray) -> Unit> = SparseArray()

    private var rxBuffer: ByteArray? = null
    private var receivedDataSize = 0

    override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
        enableNotification(rxUUID, {

            logD { "Response3: " + ByteArrayUtils.formatBytes(it) }

            if (it.isNotEmpty()){
                rxBuffer?.apply {
                    this@RxTxBleConnection.logD { "rxBuffer?.size ${rxBuffer?.size}" }
                    this@RxTxBleConnection.logD { "receivedDataSize $receivedDataSize" }
                    this@RxTxBleConnection.logD { "rxBuffer ${ByteArrayUtils.formatBytes(rxBuffer)}" }

                    receivedDataSize += if(receivedDataSize + it.size > this.size){
                        System.arraycopy(it, 0, this,
                                receivedDataSize - 1,
                                it.size - 1
                        )
                        it.size - 1
                    }else{
                        System.arraycopy(it, 0, this,
                                receivedDataSize,
                                it.size
                        )
                        it.size
                    }

                    if (receivedDataSize == this.size){
                        actions.last.response(this)
                        rxBuffer = null
                        receivedDataSize = 0
                        actions.removeLast()
                        nextTxAction()
                    }

                    return@enableNotification
                }

                if (actions.size > 0){
                    when {
                        it[0] == actions.last.txAction.rxCode.toByte() -> {
                            actions.last.apply {

                                val size: Int = if(txAction.rxSize == 2){
                                    ByteArrayUtils.unsignedBytesToInt(it[2], it[1])
                                }else{
                                    ByteArrayUtils.unsignedByteToInt(it[1])
                                }

                                if(it.size - 2 - txAction.rxSize >= size){
                                    val resp = ByteArray(size)
                                    System.arraycopy(it, txAction.rxSize + 1, resp, 0, size)
                                    actions.last.response(resp)
                                    actions.removeLast()
                                    nextTxAction()
                                }else{
                                    rxBuffer = ByteArray(size)
                                    System.arraycopy(it, txAction.rxSize + 1, rxBuffer,
                                            0,
                                            it.size - 1 - txAction.rxSize
                                    )
                                    receivedDataSize += it.size - 1 - txAction.rxSize
                                }
                            }
                        }
                        it[0] == actions.last.txAction.failRxCode.toByte() -> {
                            actions.last.fail(it)
                            actions.removeLast()
                            nextTxAction()
                        }
                        else -> {
                            rxMap[it[0].toInt()]?.invoke(it)
                            rxFailMap[it[0].toInt()]?.invoke(it)
                        }
                    }
                }

            }
        })
        super.onServicesDiscovered(gatt, status)
    }


    fun twoSum(nums: IntArray, target: Int): IntArray {
        val map = HashMap<Int, Int>()
        for (i in nums.indices) {
            val complement = target - nums[i]
            if (map.containsKey(complement)) {
                return intArrayOf(map[complement]!!, i)
            }
            map[nums[i]] = i
        }
        throw IllegalArgumentException("No two sum solution")
    }

    fun addAction(action: Action) {
        synchronized(this, {
//            logD{"stack size ${actions.size}"}
            actions.push(action)
            if (actions.size == 1) nextTxAction()
        })
    }

    private fun nextTxAction(){
//        logD{"stack size ${actions.size}"}
        if (actions.isNotEmpty()){
            val action = actions.last
            writeCharacteristic(txUUID, createRequest(action.txAction, action.data))
        }
    }

    private fun createRequest(txAction: TxAction, data: ByteArray): ByteArray {
        return joinByteArray(
                byteArrayOf(txAction.code.toByte(), data.size.toByte()),
                data,
                byteArrayOf(data.getCheckSum().toByte()))
    }

    private fun ByteArray.getCheckSum(): Int {
        var checksum = 0
        for (i in 0..lastIndex) {
            checksum = checksum xor this[i].toInt()
        }
        return checksum
    }

    data class Action(val data: ByteArray,
                      val txAction: TxAction,
                      val response: (ByteArray) -> Unit = {},
                      val fail: (ByteArray) -> Unit = {}) {

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Action

            if (!Arrays.equals(data, other.data)) return false
            if (txAction != other.txAction) return false
            if (response != other.response) return false
            if (fail != other.fail) return false

            return true
        }

        override fun hashCode(): Int {
            var result = Arrays.hashCode(data)
            result = 31 * result + txAction.hashCode()
            result = 31 * result + response.hashCode()
            result = 31 * result + fail.hashCode()
            return result
        }
    }

    companion object {
        private val txUUID = UUID.fromString("0000fff2-0000-1000-8000-00805f9b34fb")
        private val rxUUID = UUID.fromString("0000fff1-0000-1000-8000-00805f9b34fb")
    }

}

enum class TxAction(val code: Int, val rxCode: Int, val failRxCode: Int,
                    val rxSize: Int) {
    PREFERENCE(0x9B,  0x2B,0x1B,1),
    DEVICE_PREFERENCE(0xFF, 0xFF, 0x00, 1),
    DEVICE_TIME(0xC2, 0X22, 0X02, 1),
    SENSOR_SYNC(0xB1, 0xB1, 0x61, 2),
    STEP_SLEEP_SYNC(0xC4, 0x24, 0x04, 1),
    REMINDER(0xF1, 0xF1, 0x00, 1),
    APP_NOTIFICATION(0xA4, 0xA4, 0x00, 1)
}

