package com.force.emotion.app.interactor

interface ConnectionStateRepository {

    fun setConnectionState(state: Int)

    fun getConnectionState(): Int
}