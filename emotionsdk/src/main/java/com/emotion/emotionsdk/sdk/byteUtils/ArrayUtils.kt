package com.emotion.emotionsdk.sdk.byteUtils

import android.util.SparseArray
import android.util.SparseBooleanArray
import com.emotion.emotionsdk.sdk.byteUtils.Reversed.Companion.reverse
import java.util.*


fun <T : Comparable<T>> Array<out T>.random(): T{
    val rnd = Random().nextInt(this.size)
    return this[rnd]
}

fun IntArray.random(): Int{
    val rnd = Random().nextInt(this.size)
    return this[rnd]
}

inline fun <T> List<T>.reverseForEachIndexed(action: (index: Int, T) -> Unit) {
    var index = lastIndex
    for (item in reverse(this)) action(index--, item)
}

class Reversed<out T>(private val original: List<T>) : Iterable<T> {

    override fun iterator(): Iterator<T> {
        val i = original.listIterator(original.size)

        return object : Iterator<T> {
            override fun hasNext(): Boolean {
                return i.hasPrevious()
            }

            override fun next(): T {
                return i.previous()
            }
        }
    }

    companion object {

        fun <T> reverse(original: List<T>): Reversed<T> {
            return Reversed(original)
        }
    }
}

fun SparseArray<SparseBooleanArray>.putCalendar(cal: Calendar){
    var choosedDaysOfYear: SparseBooleanArray? = get(cal.get(Calendar.YEAR))

    if (choosedDaysOfYear != null) {
        val choosedDay = choosedDaysOfYear.get(cal.get(Calendar.DAY_OF_YEAR))
        choosedDaysOfYear.put(cal.get(Calendar.DAY_OF_YEAR), !choosedDay)
    } else {
        choosedDaysOfYear = SparseBooleanArray()
        put(cal.get(Calendar.YEAR), choosedDaysOfYear)
        choosedDaysOfYear.put(cal.get(Calendar.DAY_OF_YEAR), true)
    }
}

fun join(start: ByteArray, end: ByteArray): ByteArray {
    val result = ByteArray(start.size + end.size)
    System.arraycopy(start, 0, result, 0, start.size)
    System.arraycopy(end, 0, result, start.size, end.size)
    return result
}

fun addAll(array1: ByteArray, array2: ByteArray): ByteArray {
    val joinedArray = ByteArray(array1.size + array2.size)
    System.arraycopy(array1, 0, joinedArray, 0, array1.size)
    System.arraycopy(array2, 0, joinedArray, array1.size, array2.size)
    return joinedArray
}

fun joinByteArray(vararg arrays: ByteArray): ByteArray {
    var length = 0
    for (array in arrays) {
        length += array.size
    }

    val result = ByteArray(length)

    var offset = 0
    for (array in arrays) {
        System.arraycopy(array, 0, result, offset, array.size)
        offset += array.size
    }

    return result
}