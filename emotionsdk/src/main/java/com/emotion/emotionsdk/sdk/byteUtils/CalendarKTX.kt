package com.emotion.emotionsdk.sdk.byteUtils

import java.time.Year
import java.util.*

fun Calendar.getYear(): Int = get(Calendar.YEAR)
fun Calendar.getMonth(): Int = get(Calendar.MONTH)
fun Calendar.getDayOfMonth(): Int = get(Calendar.DAY_OF_MONTH)
fun Calendar.getHour(): Int = get(Calendar.HOUR_OF_DAY)
fun Calendar.getMinute(): Int = get(Calendar.MINUTE)
fun Calendar.getSecond(): Int = get(Calendar.SECOND)
fun Calendar.getDayOfWeek(): Int = get(Calendar.DAY_OF_WEEK)
