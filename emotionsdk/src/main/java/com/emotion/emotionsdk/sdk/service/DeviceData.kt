package com.emotion.emotionsdk.sdk.service

interface DeviceData {

    fun connectToDevice(address: String)
    fun disconnectFromDevice()
    fun update(otaFile: ByteArray)
    fun startMeasureHeartRate()
    fun stopMeasureHeartRate()

}