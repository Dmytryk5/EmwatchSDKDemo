package com.emotion.emotionsdk.sdk.byteUtils;


import android.util.Log;

public class ByteArrayUtils {

    /**
     * Characteristic value format type uint8
     */
    public static final int FORMAT_UINT8 = 0x11;

    /**
     * Characteristic value format type uint16
     */
    public static final int FORMAT_UINT16 = 0x12;

    /**
     * Characteristic value format type uint32
     */
    private static final int FORMAT_UINT32 = 0x14;

    /**
     * Characteristic value format type sint8
     */
    private static final int FORMAT_SINT8 = 0x21;

    /**
     * Characteristic value format type sint16
     */
    private static final int FORMAT_SINT16 = 0x22;

    /**
     * Characteristic value format type sint32
     */
    private static final int FORMAT_SINT32 = 0x24;


    public static Integer getIntValue(byte[] value, int formatType, int offset) {
        if ((offset + getTypeLen(formatType)) > value.length) return null;

        switch (formatType) {
            case FORMAT_UINT8:
                return unsignedByteToInt(value[offset]);

            case FORMAT_UINT16:
                return unsignedBytesToInt(value[offset], value[offset + 1]);

            case FORMAT_UINT32:
                return unsignedBytesToInt(value[offset], value[offset + 1],
                        value[offset + 2], value[offset + 3]);
            case FORMAT_SINT8:
                return unsignedToSigned(unsignedByteToInt(value[offset]), 8);

            case FORMAT_SINT16:
                return unsignedToSigned(unsignedBytesToInt(value[offset],
                        value[offset + 1]), 16);

            case FORMAT_SINT32:
                return unsignedToSigned(unsignedBytesToInt(value[offset],
                        value[offset + 1], value[offset + 2], value[offset + 3]), 32);
        }

        return null;
    }

    /**
     * Returns the size of a give value type.
     */
    private static int getTypeLen(int formatType) {
        return formatType & 0xF;
    }

    /**
     * Convert a signed byte to an unsigned int.
     */
    public static int unsignedByteToInt(byte b) {
        return b & 0xFF;
    }


    /**
     * Convert signed bytes to a 16-bit unsigned int.
     */
    public static int unsignedBytesToInt(byte b0, byte b1) {
        return (unsignedByteToInt(b0) + (unsignedByteToInt(b1) << 8));
    }


    /**
     * Convert signed bytes to a 32-bit unsigned int.
     */
    public static int unsignedBytesToInt(byte b0, byte b1, byte b2) {
        return (unsignedByteToInt(b0) + (unsignedByteToInt(b1) << 8))
                + (unsignedByteToInt(b2) << 16);
    }

    private static long unsignedByteToLong(byte b) {
        return b & 0xFF;
    }

    public static long unsignedBytesToLong(byte[] bytes) {
        return (unsignedByteToLong(bytes[0]) + (unsignedByteToLong(bytes[1]) << 8))
                + (unsignedByteToLong(bytes[2]) << 16)
                + (unsignedByteToLong(bytes[3]) << 24)
                + (unsignedByteToLong(bytes[4]) << 32)
                + (unsignedByteToLong(bytes[5]) << 40)
                + (unsignedByteToLong(bytes[6]) << 48)
                + (unsignedByteToLong(bytes[7]) << 56);
    }

    public static byte[] longToUnsignedByte(long n) {
        byte[] bytes = new byte[8];

        bytes[0] = (byte) (n & 0xFF);
        bytes[1] = (byte) ((n >> 8) & 0xFF);
        bytes[2] = (byte) ((n >> 16) & 0xFF);
        bytes[3] = (byte) ((n >> 24) & 0xFF);
        bytes[4] = (byte) ((n >> 32) & 0xFF);
        bytes[5] = (byte) ((n >> 40) & 0xFF);
        bytes[6] = (byte) ((n >> 48) & 0xFF);
        bytes[7] = (byte) ((n >> 56) & 0xFF);

        return bytes;
    }

    /**
     * Convert signed bytes to a 32-bit unsigned int.
     */
    public static int unsignedBytesToInt(byte b0, byte b1, byte b2, byte b3) {
        return (unsignedByteToInt(b0) + (unsignedByteToInt(b1) << 8))
                + (unsignedByteToInt(b2) << 16) + (unsignedByteToInt(b3) << 24);
    }

    public static long unsignedBytesToLong(byte b0, byte b1, byte b2, byte b3) {
        return (unsignedByteToLong(b0) + (unsignedByteToLong(b1) << 8))
                + (unsignedByteToLong(b2) << 16) + (unsignedByteToLong(b3) << 24);
    }

    public static String getBitString(byte b0) {
        return String.format("%8s", Integer.toBinaryString(b0 & 0xFF)).replace(' ', '0');
    }

//    public static byte[] getAlarmByte(Alarm alarm) {
//        byte b = (byte) (alarm.getEnabled() ? 128 : 0);
//
//        byte dayMask = alarm.getRepeatMask();
//        if (!alarm.getRepeat() && alarm.getVisible()) {
//            dayMask = (byte) 128;
//        }
//
//        return new byte[]{
//                (byte) 0x2,
//                (byte) (b + alarm.getIndex()),
//                (byte) alarm.getHour(),
//                (byte) alarm.getMinute(),
//                dayMask};
//    }

    public static String formatBytes(byte[] bytes) {
        if (bytes == null) {
            return "(null)";
        }
        StringBuilder builder = new StringBuilder(bytes.length * 5);
        for (byte b : bytes) {
//            builder.append(String.format("0x%2x", b));
//            builder.append(" ");
            builder.append(String.format("%02X ", b));
        }
        return builder.toString().trim();
    }

    public static String getDate(int date, int time) {

        int year = (date >> 28) * 1000 +
                (date >> 24 & 0x0F) * 100 +
                (date >> 20 & 0x0F) * 10 +
                (date >> 16 & 0x0F);
        int month = (date >> 12 & 0x0F) * 10 +
                (date >> 8 & 0x0F);
        int day = (date >> 4 & 0x0F) * 10 +
                (date & 0x0F);

        int hour = (time >> 28 & 0x0F) * 10 +
                (time >> 24 & 0x0F);
        int min = (time >> 20 & 0x0F) * 10 +
                (time >> 16 & 0x0F);
        int sec = (time >> 12 & 0x0F) * 10 +
                (time >> 8 & 0x0F);
        int ms = (time >> 4 & 0x0F) * 100 +
                (time & 0x0F) * 10;

        return year + " " + month + " " + day + " " + hour + " " + min + " " + sec + " " + ms;
    }


    /**
     * Convert an unsigned integer value to a two's-complement encoded
     * signed value.
     */
    private static int unsignedToSigned(int unsigned, int size) {
        if ((unsigned & (1 << size - 1)) != 0) {
            unsigned = -1 * ((1 << size - 1) - (unsigned & ((1 << size - 1) - 1)));
        }
        return unsigned;
    }

    public static int getMode(byte data){

        return data >> 6;
    }


    public static int getSteps(byte byte1, byte byte2){
        byte steps1 = (byte)(byte1 << 2);
        steps1 = (byte)(steps1 >> 2);
        int int1 = unsignedByteToInt(byte2);
        int int2 = unsignedByteToInt(steps1);
        int val;
        if (byte2 == 0){
            val = ((unsignedByteToInt(steps1)) + (unsignedByteToInt(byte2)));
        }else if (int1 == 255){
            val =  (int2) + (int1);
        }else{
            val =  (int2 << 8) + (int1);
        }

//        Log.d("GET STEPS", "byte 1 in bits: " +getBitString(byte1)
//                + " byte 2 in bits: " + getBitString(byte2)
//                + " val(returned): " + val
//                + " int1: " + int1 + " int2: " + int2 );

        return val;

    }

}
