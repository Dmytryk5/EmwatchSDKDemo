package com.emotion.emotionsdk.sdk.interactor

import com.emotion.emotionsdk.sdk.device.Device
import com.emotion.emotionsdk.sdk.interactor.ConnectionStates.Companion.CONNECTED
import com.emotion.emotionsdk.sdk.interactor.ConnectionStates.Companion.CONNECTED_OLD_VERSION
import com.emotion.emotionsdk.sdk.interactor.ConnectionStates.Companion.CONNECTING
import com.emotion.emotionsdk.sdk.interactor.ConnectionStates.Companion.DISCONNECTED
import com.emotion.emotionsdk.sdk.interactor.ConnectionStates.Companion.DISCONNECTING
import io.reactivex.disposables.CompositeDisposable


class BluetoothInteractor {


    private var characteristicDisposables: CompositeDisposable = CompositeDisposable()


    val onConnectListeners = ArrayList<OnConnectListener>()
    val onConnectOldVersionListeners = ArrayList<OnConnectOldVersionListener>()
    val onConnectingListeners = ArrayList<OnConnectingListener>()
    val onConnectionFailListeners = ArrayList<OnConnectFailListener>()
    val updateStateListeners = ArrayList<UpdateStateListener>()

    private var device: Device? = null


    fun onDeviceChooses(device: Device){
        this.device = device
    }

    fun getDeviceAddress(): String{
        return if( device != null ){
            device!!.address
        }else{
            ""
        }
    }

    fun onConnectionStateChange(connectState: Int){
        when(connectState){
            CONNECTING -> {
                onConnectingListeners.forEach { it.onConnecting()}

            }

            CONNECTED -> {
//                if( device != null) localRepository.setDeviceMac(device!!.address)
                onConnectListeners.forEach { it.onConnect() }
                characteristicDisposables.clear()

            }

            CONNECTED_OLD_VERSION -> {
//                connectionStateRepository.setConnectionState(ConnectionStates.CONNECTED_OLD_VERSION)
//                if( device != null) localRepository.setDeviceMac(device!!.address)
                onConnectOldVersionListeners.forEach { it.onOldVersionConnect() }
                characteristicDisposables.clear()


            }

            DISCONNECTED -> {
//                connectionStateRepository.setConnectionState(ConnectionStates.DISCONNECTED)
                characteristicDisposables.clear()
            }

            DISCONNECTING -> {
//                connectionStateRepository.setConnectionState(ConnectionStates.DISCONNECTING)
                characteristicDisposables.clear()
               }
        }

    }

    fun completeUpdate() {
        val currentDevice = device!!
//        connectionStateRepository.setConnectionState(ConnectionStates.DISCONNECTED)
        characteristicDisposables.clear()
        disconnectDevice()
        onDeviceChooses(currentDevice)
        updateStateListeners.forEach { it.complete() }
    }

    fun failUpdate(message: String) {
        updateStateListeners.forEach { it.fail(message) }
    }

    fun connectingFail(){
        onConnectionFailListeners.forEach { it.onConnectFail() }
        characteristicDisposables.clear()
    }

    fun disconnectDevice(){
        device = null
        characteristicDisposables.clear()
    }

    companion object {

        const val CONNECTION_STATE_CONNECTION_LOST = 0

    }

}

