package com.emotion.emotionsdk.sdk.bluetooth


data class ActivitySample(var time: Long = 0,
                          var type: Int = 0,
                          var intensity: Int = 0,
                          var steps: Int = 0,
                          var heartRate: Int = 0)

