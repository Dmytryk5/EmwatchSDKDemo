package com.emotion.emotionsdk.sdk.byteUtils

import android.util.Log
import com.emotion.emotionsdk.BuildConfig

fun Any.logD(message: String) {
    Log.d(this.javaClass.simpleName, message)
}

inline fun Any.logD(message: () -> Any?) {
    if (BuildConfig.DEBUG) Log.d(javaClass.simpleName, message().toString())
}

inline fun logd(message: () -> Any?) {
    if (BuildConfig.DEBUG) {
        val stackTrace = Thread.currentThread().stackTrace
        val className = stackTrace[2].className
        Log.d(className.substring(className.lastIndexOf('.') + 1),
                stackTrace[2].methodName + "(): " + message())
    }
}

fun Any.logE(message: String) {
    Log.e(this.javaClass.simpleName, message)
}
inline fun Any.logE(message: () -> Any?) {
    if (BuildConfig.DEBUG) Log.e(javaClass.simpleName, message().toString())
}

fun Any.logV(message: String) {
    Log.v(this.javaClass.simpleName, message)
}

fun Any.logI(message: String) {
    Log.i(this.javaClass.simpleName, message)
}

fun Any.logW(message: String) {
    Log.w(this.javaClass.simpleName, message)
}

fun Any.logWtf(message: String) {
    Log.wtf(this.javaClass.simpleName, message)
}