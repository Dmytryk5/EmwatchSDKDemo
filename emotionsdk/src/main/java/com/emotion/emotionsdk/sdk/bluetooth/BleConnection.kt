package com.emotion.emotionsdk.sdk.bluetooth

import android.bluetooth.*
import android.content.Context
import android.os.Build
import com.emotion.emotionsdk.sdk.byteUtils.ByteArrayUtils
import com.emotion.emotionsdk.sdk.bluetooth.BleConnection.BleAction.ActionType.*
import com.emotion.emotionsdk.sdk.byteUtils.logD
import java.util.*
import kotlin.collections.HashMap

open class BleConnection(private val context: Context) : BluetoothGattCallback() {

    private val bluetoothManager: BluetoothManager =
            context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
    private val bluetoothAdapter = bluetoothManager.adapter
    private var device: BluetoothDevice? = null
    private var address: String? = null
    private var bluetoothGatt: BluetoothGatt? = null
    private val actionQueue: Deque<BleAction> = ArrayDeque()
    private val onCharacteristicChangedCallbacks: HashMap<UUID, (ByteArray) -> Unit> = HashMap()
    private var connectionStateCallback: (ConnectionState) -> Unit = {}

    fun connect(macAddress: String, connectionStateCallback: (ConnectionState) -> Unit) {
        logD{"bluetooth ble connect"}
        this.address = macAddress
        this.connectionStateCallback = connectionStateCallback
        device = bluetoothAdapter.getRemoteDevice(macAddress)
        bluetoothGatt = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            device!!.connectGatt(context, false, this, 2)
        }else{
            device!!.connectGatt(context, false, this)
        }
    }

    override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
        logD("onConnectionStateChange $status")
        logD("onConnectionStateChange $newState")
        when (newState) {
            BluetoothProfile.STATE_DISCONNECTED -> {
                logD("STATE_DISCONNECTED")
                bluetoothGatt?.disconnect()
                bluetoothGatt?.close()
                bluetoothGatt = null
                device = null
                if (actionQueue.size != 0 && actionQueue.last.type == DISCONNECT) {
                    logD("STATE_DISCONNECTED1")
                    bluetoothGatt?.disconnect()
                    bluetoothGatt?.close()
                    bluetoothGatt = null
                    device = null
                }

                connectionStateCallback(ConnectionState.STATE_DISCONNECTED)
            }

            BluetoothProfile.STATE_CONNECTING ->{
                logD("STATE_CONNECTING")
                connectionStateCallback(ConnectionState.STATE_CONNECTING)
            }

            BluetoothProfile.STATE_CONNECTED -> {
                logD("STATE_CONNECTED")
                val cachedServices = gatt.services
                if (cachedServices != null && cachedServices.size > 0) {
                    logD("Using cached services, skipping discovery")
                    onServicesDiscovered(gatt, BluetoothGatt.GATT_SUCCESS)
                } else {
                    Thread.sleep(1000)
                    logD("Attempting to start service discovery:" + gatt.discoverServices())
                }
            }


            BluetoothProfile.STATE_DISCONNECTING ->
                connectionStateCallback(ConnectionState.STATE_DISCONNECTING)
        }
    }

    override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
        if (status == BluetoothGatt.GATT_SUCCESS) {
            connectionStateCallback(ConnectionState.STATE_CONNECTED)
        } else {
            logD("onServicesDiscovered received: $status")
        }
    }

    override fun onCharacteristicRead(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic, status: Int) {

//        logD{"stack size ${actionQueue.size}"}

        logD("onCharacteristicRead UUID: ${characteristic.uuid}," +
                " value: ${ByteArrayUtils.formatBytes(characteristic.value)}")

        if (actionQueue.size != 0 && actionQueue.last.characteristic == characteristic.uuid) {
            actionQueue.last.callback?.invoke(characteristic.value)
            actionQueue.removeLast()
            nextAction()
        }
    }

    override fun onCharacteristicWrite(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic, status: Int) {

        logD("onCharacteristicWrite UUID: ${characteristic.uuid}," +
                " value: ${ByteArrayUtils.formatBytes(characteristic.value)}")

//        logD{"stack size ${actionQueue.size}"}

        if (actionQueue.size != 0 && actionQueue.last.characteristic == characteristic.uuid) {
            actionQueue.last.callback?.invoke(characteristic.value)
            actionQueue.removeLast()
            nextAction()
        }
    }

    override fun onCharacteristicChanged(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic) {
        logD("onCharacteristicChanged UUID: ${characteristic.uuid}," +
                " value: ${ByteArrayUtils.formatBytes(characteristic.value)}")
//        logD{"stack size ${actionQueue.size}"}
        onCharacteristicChangedCallbacks[characteristic.uuid]?.invoke(characteristic.value)

    }

    override fun onDescriptorWrite(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int) {

        if (actionQueue.size != 0) {
            val action = actionQueue.last
            if (action.characteristic == descriptor.characteristic.uuid) {
                if (actionQueue.last.type == ENABLE_NOTIFICATION) {
                    if (action.callback != null) {
                        if (descriptor.value!!.contentEquals(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE)) {
                            onCharacteristicChangedCallbacks[action.characteristic!!] = action.callback
                        } else if (descriptor.value!!.contentEquals(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE)) {
                            onCharacteristicChangedCallbacks.remove(action.characteristic!!)
                        }
                    }
                    actionQueue.removeLast()
                    nextAction()
                }
            }
        }
    }

    fun writeCharacteristic(characteristic: UUID, value: ByteArray, callback: (ByteArray) -> Unit = {}) {
        synchronized(this, {
            actionQueue.push(BleAction(WRITE_CHARACTERISTIC, characteristic, value, callback))
            if (actionQueue.size == 1) nextAction()
        })
    }

    fun readCharacteristic(characteristic: UUID, callback: (ByteArray) -> Unit, value: ByteArray? = null) {
        synchronized(this, {
            actionQueue.push(BleAction(READ_CHARACTERISTIC, characteristic, value, callback))
            if (actionQueue.size == 1) nextAction()
        })
    }

    fun enableNotification(characteristic: UUID, callback: ((ByteArray) -> Unit)? = null, enable: Boolean = true) {
        synchronized(this, {
            actionQueue.push(BleAction(ENABLE_NOTIFICATION, characteristic, byteArrayOf(if (enable) 1 else 0), callback))
            if (actionQueue.size == 1) nextAction()
        })
    }

    fun disconnect() {
        synchronized(this, {
            actionQueue.push(BleAction(DISCONNECT))
            if (actionQueue.size == 1) nextAction()
        })
    }

    private fun nextAction() {
        if (actionQueue.isNotEmpty()) {
            when (actionQueue.last.type) {
                WRITE_CHARACTERISTIC -> writeCharacteristic()
                READ_CHARACTERISTIC -> readCharacteristic()
                ENABLE_NOTIFICATION -> enableNotification()
                DISCONNECT -> disconnectFromDevice()
            }
        }
    }

    private fun getCharacteristic(characteristic: UUID): BluetoothGattCharacteristic? {
        bluetoothGatt?.services?.forEach {
            it.characteristics.forEach {
                if (characteristic == it.uuid) return it
            }
        }
        return null
    }

    private fun writeCharacteristic() {
        val action = actionQueue.last
        getCharacteristic(action.characteristic!!)?.apply {
            value = action.value
            bluetoothGatt?.writeCharacteristic(this)
        }

    }

    private fun readCharacteristic() {
        val action = actionQueue.last
        getCharacteristic(action.characteristic!!)?.apply {
            if (action.value != null) value = action.value
            bluetoothGatt?.readCharacteristic(this)
        }

    }

    private fun enableNotification() {
        val action = actionQueue.last
        val enable = action.value!![0].toInt() == 1
        getCharacteristic(action.characteristic!!)?.apply {
            bluetoothGatt?.setCharacteristicNotification(this, enable)
            val descriptor = getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"))
            if (enable) descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            else descriptor.value = BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE
            bluetoothGatt?.writeDescriptor(descriptor)
        }

    }

    private fun disconnectFromDevice() {
        bluetoothGatt?.disconnect()
    }

    enum class ConnectionState {
        STATE_DISCONNECTED,
        STATE_CONNECTING,
        STATE_CONNECTED,
        STATE_DISCONNECTING,
    }

    private data class BleAction(val type: ActionType,
                                 val characteristic: UUID? = null,
                                 val value: ByteArray? = null,
                                 val callback: ((ByteArray) -> Unit)? = null) {
        enum class ActionType {
            WRITE_CHARACTERISTIC,
            READ_CHARACTERISTIC,
            ENABLE_NOTIFICATION,
            DISCONNECT,
        }


        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as BleAction

            if (type != other.type) return false
            if (characteristic != other.characteristic) return false
            if (!Arrays.equals(value, other.value)) return false
            if (callback != other.callback) return false

            return true
        }

        override fun hashCode(): Int {
            var result = type.hashCode()
            result = 31 * result + (characteristic?.hashCode() ?: 0)
            result = 31 * result + (value?.let { Arrays.hashCode(it) } ?: 0)
            result = 31 * result + (callback?.hashCode() ?: 0)
            return result
        }


    }


}