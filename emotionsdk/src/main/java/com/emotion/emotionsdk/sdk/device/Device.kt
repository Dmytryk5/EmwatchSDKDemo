package com.emotion.emotionsdk.sdk.device

import com.emotion.emotionsdk.sdk.device.Device.Companion.XIAOMI_MACS

data class Device(val name: String?, val address: String) {
    companion object {

        val XIAOMI_MACS = hashMapOf(Pair("",""))
    }
}

fun Device.isEmTracker(): Boolean{
    return name!!.toLowerCase().contains("EmTracker".toLowerCase()) ||
            name.toLowerCase().contains("OTA".toLowerCase())
}

fun Device.isXiaomi(): Boolean{
    return name!!.toLowerCase().replace(" ", "")
            .contains("Amazfit Cor".toLowerCase().replace(" ", "")) ||
            name.toLowerCase().replace(" ", "")
                    .contains("MI Band 2".toLowerCase().replace(" ", "")) ||
            name.toLowerCase().replace(" ", "")
                    .contains("HM0D".toLowerCase().replace(" ", ""))

}

fun Device.mapToCorporetCorporateEdition(): Device{
    return if(XIAOMI_MACS[address] != null){
        Device(name = XIAOMI_MACS[address], address = address)
    }else{
        this
    }
}