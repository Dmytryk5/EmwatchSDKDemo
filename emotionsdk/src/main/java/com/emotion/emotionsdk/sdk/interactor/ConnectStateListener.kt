package com.emotion.emotionsdk.sdk.interactor


interface OnConnectListener {

    fun onConnect()
}

interface OnConnectOldVersionListener {

    fun onOldVersionConnect()

}

interface OnConnectFailListener{

    fun onConnectFail()

}

interface OnDisconnectListener{

    fun onDisconnect()

}

interface OnReconnectingListener{

    fun onReconnecting()

}

interface OnDisconnectingListener{

    fun onDisconnecting()

}

interface OnConnectingListener{

    fun onConnecting()

}

interface OnUserStateChangeListener{

    fun onUserStateChange(stress: Int, stamina: Int, stateId: Int)

}