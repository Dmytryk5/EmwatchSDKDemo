package com.emotion.emotionsdk.sdk.interactor

interface UpdateStateListener {

    fun complete()

    fun fail(message: String)

}

interface UpdateAvailableListener {

    fun updateAvailable(b: Boolean)

}