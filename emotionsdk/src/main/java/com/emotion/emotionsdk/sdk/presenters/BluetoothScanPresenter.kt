package com.emotion.emotionsdk.sdk.presenters

import com.emotion.emotionsdk.sdk.device.Device
import com.emotion.emotionsdk.sdk.interactor.BluetoothInteractor
import com.emotion.emotionsdk.sdk.interactor.OnConnectFailListener
import com.emotion.emotionsdk.sdk.interactor.OnConnectListener
import com.emotion.emotionsdk.sdk.interactor.OnConnectOldVersionListener
import io.reactivex.disposables.CompositeDisposable




class BluetoothScanPresenter: OnConnectListener, OnConnectOldVersionListener, OnConnectFailListener {

    private var view: BluetoothScanView? = null
    lateinit var bluetoothInteractor: BluetoothInteractor

//    private val compositeDisposable : CompositeDisposable = CompositeDisposable()

    fun bindView(view: BluetoothScanView){
        this.view = view
        bluetoothInteractor.onConnectListeners.add(this)
        bluetoothInteractor.onConnectionFailListeners.add(this)
        bluetoothInteractor.onConnectOldVersionListeners.add(this)
    }

//    fun loadDevices() {
//        compositeDisposable.add(bluetoothScanRepository
//                .getBluetoothDeviceObservable()
//                .filter { Device.XIAOMI_MACS[it.address] != null || it.name != null &&
//                        (it.name.toLowerCase().contains("EM Band".toLowerCase()) ||
//                        it.name.toLowerCase().contains("CC Band".toLowerCase()) ||
//                    it.name.toLowerCase().contains("EMwatch".toLowerCase()))
//                }
//                .map{ it.mapToCorporetCorporateEdition() }
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe({
//                    view?.addDevice(it)
//                },{
//                    //log exception
//                })
//        )
//    }

    fun onDeviceChoose(device: Device){
        bluetoothInteractor.onDeviceChooses(device)
    }

    fun abortConnection(){
        bluetoothInteractor.disconnectDevice()
    }

    override fun onConnect() {
        view?.onConnect()
    }

    override fun onOldVersionConnect() {
        view?.onOldVersionConnect()
    }

    override fun onConnectFail() {
        view?.onConnectFail()
    }

    fun unbindView(){
        view = null
    }

    fun onDestroyView(){
        bluetoothInteractor.onConnectListeners.remove(this)
        bluetoothInteractor.onConnectionFailListeners.remove(this)
        bluetoothInteractor.onConnectOldVersionListeners.remove(this)
//        compositeDisposable.clear()
    }

}

interface BluetoothScanView{

    fun addDevices(devices: Set<Device>)
    fun addDevice(device: Device)
    fun onConnect()
    fun onOldVersionConnect()
    fun onConnectFail()
    fun locationDisabled()
    fun unknownError()

}