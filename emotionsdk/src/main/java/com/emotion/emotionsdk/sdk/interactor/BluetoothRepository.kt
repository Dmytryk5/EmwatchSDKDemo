package com.emotion.emotionsdk.sdk.interactor

import io.reactivex.Observable
import io.reactivex.flowables.ConnectableFlowable

interface BluetoothRepository{

    fun getConnectionStateObservable(): Observable<Int>

//    fun getHeartRateFlowable(): ConnectableFlowable<HeartRate>

    fun startMeasureHeartRate()
    fun stopMeasureHeartRate()

//    fun getMotionActivityFlowable(): ConnectableFlowable<MotionActivity>

//    fun getEdaFlowable(): ConnectableFlowable<Eda>

//    fun getStepsFlowable(): ConnectableFlowable<Steps>

    fun getFirmwareVersion(): Int

    fun disconnect()

    fun update(otaFile: ByteArray)
}
