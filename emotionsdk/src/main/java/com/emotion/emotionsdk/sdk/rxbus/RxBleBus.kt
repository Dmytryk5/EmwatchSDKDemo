package com.emotion.emotionsdk.sdk.rxbus

import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.subjects.PublishSubject

object RxBleBus {

    private val publisher = PublishSubject.create<Any>()

    fun publish(event: Any) {
        publisher.onNext(event)
    }

    fun <T> listen(eventType: Class<T>): Flowable<T> = publisher.ofType(eventType)
            .toFlowable(BackpressureStrategy.LATEST)

}