package com.emotion.emotionsdk.sdk.interactor


class ConnectionStates {

    companion object {

        const val SERVICE_NO_STARTED = 0

        const val CONNECTING = 1

        const val CONNECTED  = 2

        const val CONNECTED_OLD_VERSION = -1

        const val RECONNECTING  = 3

        const val DISCONNECTED = 4

        const val DISCONNECTING = 5

    }

}