package com.emotion.emotionsdk.sdk.bluetooth

import android.content.Context
import com.emotion.emotionsdk.sdk.byteUtils.*
import com.emotion.emotionsdk.sdk.rxbus.ReadLogAction
import com.emotion.emotionsdk.sdk.rxbus.RxBleBus
import com.emotion.emotionsdk.sdk.service.DeviceData
import io.reactivex.disposables.CompositeDisposable
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class EmWatchDevice(val address: String, private val context: Context) : DeviceData {
    private var bleConnection: RxTxBleConnection? = null
    private var selfDisconnect: Boolean = true
    private val compositeDisposable = CompositeDisposable()
    private var cal = GregorianCalendar.getInstance()
    private var currProgress = 0
//    private var battery: Int = 0
    private val activities = mutableListOf<ActivitySample>()
    private var synchronizedWithDevice = false
    private var connected = false
    private val startCalendar = Calendar.getInstance()
    var syncListener:DeviceSynchronizationListener? = null

    init {
        startCalendar.timeInMillis = cal.timeInMillis - (30 * 60 * 60 * 1000)
    }

    fun connectToEmWatch(){
        connectToDevice(this.address)
    }

    fun startReadTimeInMillis(timeInMillis: Long){
        startCalendar.timeInMillis = timeInMillis
    }

    override fun disconnectFromDevice() {
        logD{"Disconnected from Device"}
        selfDisconnect = false
        connected = false
        bleConnection?.disconnect()
    }

    override fun update(otaFile: ByteArray) {
    }

    override fun startMeasureHeartRate() {
    }

    override fun stopMeasureHeartRate() {
    }


    override fun connectToDevice(address: String) {
        logD{"Connecting"}
        bleConnection = RxTxBleConnection(this.context)
        bleConnection?.connect(address) {
            when (it) {
                BleConnection.ConnectionState.STATE_DISCONNECTED -> {
                    if (selfDisconnect) {
                        logD{"disconnected"}
//                        reconnect()
                    } else {
                        logD{"disconnected"}
                    }
                }
                BleConnection.ConnectionState.STATE_CONNECTING -> {
                    logD{"connecting"}
                }
                BleConnection.ConnectionState.STATE_CONNECTED -> {
                    logD{"connected to emwatch"}

                    onConnect()
                }
                BleConnection.ConnectionState.STATE_DISCONNECTING -> {
                    logD{"disconnecting"}
                }
            }
        }
    }

    private fun onConnect() {
        logD { "onConnect" }

        connected = true

        bleConnection?.readCharacteristic(
                UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb"), {
                        val battery = it[0].toInt()
                        if (syncListener!= null) syncListener?.onBatteryRead(battery)
                        logD { "update battery $battery "}
        })


        bleConnection?.addAction(RxTxBleConnection.Action(getTime(),
                TxAction.DEVICE_TIME, { logD { "TIME WRITE" } }, { logD { "TIME WRITE FAIL" } }))

        bleConnection?.addAction(RxTxBleConnection.Action(getPreference(),
                TxAction.PREFERENCE, { logD { "PREFERENCE WRITE" } }, { logD { "PREFERENCE WRITE FAIL" } }))

        bleConnection?.addAction(RxTxBleConnection.Action(getDevicePreference(),
                TxAction.DEVICE_PREFERENCE, {
            logD { "DEVICE_PREFERENCE WRITE" }
            if (syncListener!= null) syncListener?.onDeviceConnected()

        }, { logD { "DEVICE_PREFERENCE WRITE FAIL" } }))



//
//        cal = GregorianCalendar.getInstance() //put your start here
//        cal.timeInMillis = cal.timeInMillis - (30 * 60 * 60 * 1000)
//        cal.timeInMillis = 1534377600000L

        receiveStepsAndSleepData(startCalendar)


    }


    private fun getDevicePreference(): ByteArray {
        return byteArrayOf(
                0,
                0,
                0,
                0,
                0,
                0,
                1,
                1,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
        )
    }

    private fun getTime(): ByteArray {
        val c = Calendar.getInstance()
        c.firstDayOfWeek = Calendar.MONDAY
        var dayOfWeek = c.getDayOfWeek() - 1
        if (dayOfWeek == 0) dayOfWeek = 7

        //debug only
//        dayOfWeek = 5
//        return byteArrayOf(
//                (c.getYear() % 100).toByte(),
//                (c.getMonth() - 1).toByte(),
//                (c.getDayOfMonth() - 6).toByte(),
//                (c.getHour() - 2).toByte(),
//                (c.getMinute() -12).toByte(),
//                (c.getSecond()).toByte(),
//                dayOfWeek.toByte()
//        )


        //true code
        return byteArrayOf(
                (c.getYear() % 100).toByte(),
                (c.getMonth() + 1).toByte(),
                (c.getDayOfMonth()).toByte(),
                (c.getHour()).toByte(),
                (c.getMinute()).toByte(),
                (c.getSecond()).toByte(),
                dayOfWeek.toByte()
        )
    }

    private fun getPreference(): ByteArray {
        //EmWatch preferences. Some of them might not work

        val gesture = true//sharedPrefs.getBoolean(
//                getString(R.string.preference_gesture_activation), true)

        val format = true//sharedPrefs.getBoolean(
//                getString(R.string.preference_format), true)


        val gestureByte: Byte = if (gesture) 1 else 0
        val formatByte: Byte = if (format) 1 else 0

        //1 - Ukr, 0 - Eng
        val langUkrByte: Byte = 1//if (Locale.getDefault().language == "uk") 1 else 0

        return byteArrayOf(
                formatByte, //12 hours 00,24 hours 01
                0, //Celsius 00, Fahrenheit 01
                30, //Screen-off time setting, in seconds. Minimum 0, maximum 255
                1, //Positive and negative screen, positive screen 00, negative screen 01
                1, //Unit of measurement 1: Metric 2: Imperial
                1, //Vibration 0: Vibration off 1: Vibration on 2:Anti-lost vibration off
                gestureByte, //Turning screen display on upon flipping hand (motion gesture) 0: Off 1: On
                langUkrByte, //Language Selection 0: English 1: Chinese 2: Russian 3: Ukrainian Default: 0
                0, //Do not disturb (Quiet) mode 0: Off 1: On
                22, //Quiet Mode Starting Hours
                0, //Quiet Mode Starting Minutes
                8, //Quiet Mode Ending Hours
                0  //Quiet Mode Ending Minutes
        )
    }

    private fun syncData(){
        val startTimestamp = cal
        bleConnection?.addAction(RxTxBleConnection.Action(
                getSyncData(startTimestamp),
                TxAction.STEP_SLEEP_SYNC,
                {
                    logD {
                        ByteArrayUtils.formatBytes(it)
                    }
                    parseDataLog(it, startTimestamp)
                }, {
            val calendar = GregorianCalendar()   // check if log time not current
            val secondStartTimeStamp = startTimestamp.timeInMillis + 1000L * 60L * 60L * 3
            if ((calendar.timeInMillis - secondStartTimeStamp) > 1000L * 60L * 20){
//                localRepository.putLong(NewXiaomiDeviceService.LAST_ACTIVITY_SYNC_DATE_KEY,
//                        secondStartTimeStamp)
                cal.timeInMillis = secondStartTimeStamp
                syncData()
            }
        }))

    }

    private fun getSyncData(calendar: Calendar): ByteArray {
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        calendar.setFirstDayOfWeek(2)
        var dayOfWeek = e(calendar.getTime());
        if (dayOfWeek == 0) dayOfWeek = 7

//        logD { "dayOfWeek $dayOfWeek original dayOfWeek ${calendar.getDayOfWeek()}" }
//        logD{ ByteArrayUtils.formatBytes(byteArrayOf(dayOfWeek.toByte(), calendar.getHour().toByte(), 3.toByte()))}
        return byteArrayOf(dayOfWeek.toByte(), calendar.getHour().toByte(), 3.toByte())
    }

    fun e(theDay: Date): Int {
        val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val testDay = df.format(theDay)
        val cal = Calendar.getInstance()
        cal.time = theDay
        val year = cal.get(1)
        val century = cal.get(1) / 100
        val year2 = cal.get(1) % 100
        val month = cal.get(2)
        val day = cal.get(5)
        val mon_th: Int
        val ye_ar: Int
        val centu_ry: Int
        if (month + 1 != 1 && month + 1 != 2) {
            mon_th = month + 1
            ye_ar = year % 100
            centu_ry = year / 100
        } else {
            mon_th = month + 1 + 12
            ye_ar = (year - 1) % 100
            centu_ry = (year - 1) / 100
        }

        val week = ye_ar + ye_ar / 4 + centu_ry / 4 - 2 * centu_ry + 26 * (mon_th + 1) / 10 + day - 1
        val week2 = week and 255
        var weekday = week2 % 7
        if (weekday == 0) {
            weekday = 7
        }

        return weekday
    }



    private fun receiveStepsAndSleepData(startTimestamp: Calendar) {

        val activity = ArrayList<ActivitySample>()
        syncData(startTimestamp, activity)
        dataSync(66)
        compositeDisposable.add(
                RxBleBus.listen(ReadLogAction::class.java)
                        .subscribe({
                            syncData(startTimestamp, activity)
                        }, {
                            logD{it} })
        )


    }




    private fun parseDataLog(log: ByteArray, startTimestamp: Calendar, activity: ArrayList<ActivitySample>){
        val boundary = if (log.size < 98) log.size else 98

        val byteBuffer = ByteBuffer.allocate(6)

        loop@ for (i in 4 until boundary step 6) { //
            val chunk = byteArrayOf(log[i], log[i+1])//actual data portion
            val oldType = ByteArrayUtils.getMode(log[i])
            val oldSteps = ByteArrayUtils.getSteps(log[i], log[i+1])

            byteBuffer.clear()
            byteBuffer.put(chunk)
            byteBuffer.flip() //switch buffer to read mode
            byteBuffer.order(ByteOrder.BIG_ENDIAN)//first short must be MSB

            val dataShort : Short = byteBuffer.getShort() //Trusang bluetooth protocol page 23-24
            val dataShortAsInt : Int = dataShort.toInt()

            byteBuffer.flip()//switch buffer to write mode for next loop

            val type : Int = dataShortAsInt shr 14 //BigEndian

            val activityData : Int = dataShortAsInt and 0x3FFF //erases data after 14th bit

            if (activityData > 5000){
                if(oldType == 0){
                    activity.add(ActivitySample(startTimestamp.timeInMillis, 1, 4, oldSteps, 0))
                }
                else {
                    activity.add(ActivitySample(startTimestamp.timeInMillis, 9, 0, 0, 0))
                }

//                logD { startTimestamp.time }
//                logD { activity[activity.lastIndex] }
                startTimestamp.add(Calendar.MINUTE, 10)
                continue@loop

            }

            val activityType = if (type == 1) 9 else if (type == 0) 1 else 4  //type = 9 - sleep mode; type = 1 - daily mode 4 - error

            val activityIntensity = //intensity is miBand only characteristic
                    if (type == 1){ //type 1 = sleep
                        if(activityData == 100 || activityData == 20 || activityData == 0) { //sleep types
                            activityData
                        }else{
                            4 //4 is for error
                        }
                    } else if (type == 0) 0 else 4 //miBand only; if 4 - error

            val activityValue =
                    if (type == 1) {
                        if (activityData == 100 || activityData == 20 || activityData == 0) {
                            activityData
                        } else {
                            0
                        }
                    } else{
                        if (type == 0) { //type 0 is daily mode
                            activityData //when type = 0 activityData is steps count
                        }
                        else{
                            0
                        }
                    }

            activities.add(
                    ActivitySample(
                            startTimestamp.timeInMillis,
                            activityType,
                            activityIntensity,
                            activityValue,
                            0 //to obtain this information we need another request(0xB1, 0x93)
                    )
            )
//            logD { "Reading data from device:"}
//            logD { startTimestamp.time }
//            logD { activities[activities.lastIndex] }

//            activities.add(activity)
            startTimestamp.add(Calendar.MINUTE, 10)

        }


        val calendar = GregorianCalendar()   // check if log time not current
        if ((calendar.timeInMillis - startTimestamp.timeInMillis) > 1000L * 60L * 20){

            calendar.timeInMillis = startTimestamp.timeInMillis
            syncData(calendar, activity)
        }else{
            dataSync(100)
        }

    }

    private fun parseDataLog(log: ByteArray, startTimestamp: Calendar) {

        val activity = ArrayList<ActivitySample>()

        logD { "ByteArrayLog parseDataLog: " + ByteArrayUtils.formatBytes(log) +
                " :end byteArray. size = ${log.size}" }

        val byteBuffer = ByteBuffer.allocate(6)



        loop@ for (i in 4 until log.size step 6) {
            val chunk = byteArrayOf(log[i], log[i+1])//actual data portion

            logD{"NEW ENTRY"}

            val oldType = ByteArrayUtils.getMode(log[i])
            val oldSteps = ByteArrayUtils.getSteps(log[i], log[i+1])

            byteBuffer.clear()
            byteBuffer.put(chunk)

            byteBuffer.flip() //switch buffer to read mode

            byteBuffer.order(ByteOrder.BIG_ENDIAN)//first short must be MSB
            val dataShort : Short = byteBuffer.getShort() //Trusang bluetooth protocol page 23-24
            val dataShortAsInt : Int = dataShort.toInt()

            byteBuffer.flip()//switch buffer to write mode for next loop

            val type : Int = dataShortAsInt shr 14 //BigEndian
            val activityData : Int = dataShortAsInt and 0x3FFF //erases data after 14th bit

            if (activityData > 5000){
                if(oldType == 0){
                    activity.add(ActivitySample(startTimestamp.timeInMillis, 1, 4, oldSteps, 0))
                }
                else {
                    activity.add(ActivitySample(startTimestamp.timeInMillis, 9, 0, 0, 0))
                }


                logD { startTimestamp.time }
                logD { activity[activity.lastIndex] }
                startTimestamp.add(Calendar.MINUTE, 10)
                continue@loop

            }
//

            val activityType = if (type == 1) 9 else if (type == 0) 1 else 4  //type = 1 saved as 9 - sleep mode; type = 0 saved as 1 - daily mode; 4 - error

            val activityIntensity = //intensity is miBand only characteristic
                    if (type == 1){ //type 1 = sleep
                        if(activityData == 100 || activityData == 20 || activityData == 0) { //sleep types
                            activityData
                        }else{
                            4 //4 is for error
                        }
                    } else if (type == 0) 0 else 4 //miBand only; if 4 - error

            //100(0x64) - deep sleep, 20 - mid sleep, 0 - no sleep
            val activityValue =
                    if (type == 1) {//type 1 is sleep mode
                        if (activityData == 100 || activityData == 20 || activityData == 0) {
                            activityData //when type = 1 activityData is one of sleep characteristics
                        } else {
                            4
                        }
                    } else{
                        if (type == 0) { //type 0 is daily mode
                            activityData //when type = 0 activityData is steps count
                        }
                        else{
                            4
                        }
                    }


            activity.add(
                    ActivitySample(
                            startTimestamp.timeInMillis,
                            activityType,
                            activityIntensity,
                            activityValue,
                            0 //to obtain this information we need another request(0xB1, 0x93)
                    )
            )

            logD { startTimestamp.time }
            logD { activity[activity.lastIndex] }
            startTimestamp.add(Calendar.MINUTE, 10)

        }


        val calendar = GregorianCalendar()   // check if log time not current
        if ((calendar.timeInMillis - startTimestamp.timeInMillis) > 1000L * 60L * 20){
            calendar.timeInMillis = startTimestamp.timeInMillis
            cal.timeInMillis = startTimestamp.timeInMillis
            syncData()
        }else{
            dataSync(100)
        }

    }

    private fun dataSync(progress: Int) {
        logD { "dataSync" }
        if (currProgress != progress) {

            currProgress = progress

            if (progress == 100) {

                val tmp = activities.distinct()


                logD { "data Synchronized activities size: ${tmp.size}" }


                synchronizedWithDevice = true
                activities.clear()
                activities.addAll(tmp)

//                activities.forEach {
//                    val cal = Calendar.getInstance()
//                    cal.timeInMillis = it.time
//
//                    logD{"${cal.time} \n $it"}
//                }
                //activities ^

                val cal = Calendar.getInstance()
                val currentTimeLong = cal.timeInMillis

                activities.reverse()

                activities.forEach {
                    if (currentTimeLong < it.time)
                        activities.remove(it)
                    else return@forEach
                }

                activities.reverse()

                if (syncListener!= null) syncListener?.onActivitySynchronized(activities)
            }
        }
    }


    //get sleep and steps data from connected EmWatch starting from startTimestamp
    private fun syncData(startTimestamp: Calendar, activity: ArrayList<ActivitySample>){
//        val startTimestamp = getLastSuccessfulSyncTime()
        bleConnection?.addAction(RxTxBleConnection.Action(
                getSyncData(startTimestamp),
                TxAction.STEP_SLEEP_SYNC,
                {
                     parseDataLog(it, startTimestamp, activity)
                }, {
            val calendar = GregorianCalendar()   // check if log time not current
            val secondStartTimeStamp = startTimestamp.timeInMillis + 1000L * 60L * 60L * 3
            if ((calendar.timeInMillis - secondStartTimeStamp) > 1000L * 60L * 20){
                calendar.timeInMillis = secondStartTimeStamp
                 syncData(calendar, activity)

            }
        }))

    }

    public interface DeviceSynchronizationListener{
        public fun onDeviceConnected()
        public fun onActivitySynchronized(activities: MutableList<ActivitySample>)
        public fun onBatteryRead(batteryLevel: Int)

    }

}