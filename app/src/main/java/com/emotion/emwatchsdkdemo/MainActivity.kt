package com.emotion.emwatchsdkdemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.emotion.emotionsdk.sdk.bluetooth.*
import com.emotion.emotionsdk.sdk.byteUtils.logD
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {

    private val address = "FB:A7:09:8B:ED:0C"
    private val emWatch = EmWatchDevice(this.address, this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tvList.text = "Not connected"

        buttonConnect.setOnClickListener {
            val emWatch = EmWatchDevice(address, this)

            tvList.text = "Connecting"

            emWatch.syncListener = object : EmWatchDevice.DeviceSynchronizationListener{
                override fun onDeviceConnected() {
                    runOnUiThread {
                        tvList.text = "Connected to Device" }
                }

                override fun onActivitySynchronized(activities: MutableList<ActivitySample>) {
                    this@MainActivity.runOnUiThread {
                        activities.forEach {
                            val cal = Calendar.getInstance()
                            cal.timeInMillis = it.time
                            tvList.append("\n${cal.time} \n $it")
                        }
                    }
                }

                override fun onBatteryRead(batteryLevel: Int) {
                    runOnUiThread{
//                        tvList.append("Battery level: $batteryLevel")
                    }
                }

            }

            emWatch.connectToEmWatch()
        }

    }


    override fun onDestroy() {
        emWatch.disconnectFromDevice()
        super.onDestroy()
    }
}
